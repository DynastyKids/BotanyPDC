<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Parcellist $parcellist
 */
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/manages">Sorting List Manager</a>
    </div>
</nav>
<div class="container">
    <?= $this->Form->create($parcellist) ?>
    <fieldset>
        <legend><?= __('Edit Street Details') ?></legend>
        <?= $this->Form->control('streetname',['label'=>'Street Name:','type'=>'text','class'=>'form-control']);?>
        <br>
        <?= $this->Form->control('ndriver',['label'=>'Driver','type'=>'text','class'=>'form-control','placeholder'=>'*** Type in new name. Current Driver: '.$parcellist->driver]);?>
        <small>Drivers in list:
            <?php for($ii=0;$ii<sizeof($driverlist);$ii++){
                if(strpos($driverlist[$ii]['driver'], "SORT") == false && strpos($driverlist[$ii]['driver'], "sort") == false){
                    echo $driverlist[$ii]['driver']." | ";
                }
            }?>
        </small>
        <br><br>
        <?php
        $duplicatecount = \Cake\ORM\TableRegistry::getTableLocator()->get('parcellist')->find()->where(['streetname'=>$parcellist->streetname])->count();
        if($duplicatecount>0) {
            echo $this->Form->control('displayorder', ['label' => 'Set Display Order:', 'type' => 'number', 'class' => 'form-control', 'style' => 'max-width:100px']);
        }
        ?>
        <br>

        <label class="col-form-label">Odd Number Range:</label><br>
        <div class="row">
            <div class="col-3"><?= $this->Form->control('oddblimit',['label'=>'From','type'=>'number','id'=>'oddblimit','class'=>'form-control','style'=>'max-width:100px','default'=>1])?></div>
            <div class="col-3"><?= $this->Form->control('oddulimit',['label'=>'To:','type'=>'number','id'=>'oddulimit','class'=>'form-control','style'=>'max-width:100px','default'=>999])?></div>
            <button onclick="excludeodd()" type="button" class="col-3 btn btn-warning">Exclude Odd Numbers</button>
        </div>
        <br>
        <div class="row">
            <label class="col-form-label">Even Number Range</label>
            <div class="col-3"><?= $this->Form->control('evenblimit',['label'=>'From:','type'=>'number','id'=>'evenblimit','class'=>'form-control col-sm-2','style'=>'max-width:100px','default'=>2])?></div>
            <div class="col-3"><?= $this->Form->control('evenulimit',['label'=>'To:','type'=>'number','id'=>'evenulimit','class'=>'form-control col-sm-2','style'=>'max-width:100px','default'=>998])?></div>
            <button onclick="excludeeven()" type="button" class="col-3 btn btn-warning">Exclude Even Numbers</button>
        </div>
        <br>
        <?= $this->Form->hidden('zone',['label'=>'Zone Name:','type'=>'text','class'=>'form-control','value'=>'randwick']);?>
    </fieldset>
    <br>
    <?= $this->Form->button('Submit',['class'=>'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>


<script>
    function excludeodd() {
        document.getElementById("oddblimit").value = 0;
        document.getElementById("oddulimit").value = 0;
    }
</script>

<script>
    function excludeeven() {
        document.getElementById("evenblimit").value = 0;
        document.getElementById("evenulimit").value = 0;
    }
</script>
